<?php
// +----------------------------------------------------------------------
// | RXThinkCMF_EVTP6_PRO前后端分离旗舰版框架 [ RXThinkCMF ]
// +----------------------------------------------------------------------
// | 版权所有 2021 南京RXThinkCMF研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.rxthink.cn
// +----------------------------------------------------------------------
// | 作者: 牧羊人 <rxthinkcmf@163.com>
// +----------------------------------------------------------------------
// | 免责声明:
// | 本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违
// | 反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外
// | 、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带
// | 或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允
// | 许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace app\admin\middleware;

use think\Response;

/**
 * 登录校验中间件
 * @author 牧羊人
 * @since 2021/1/8
 * Class CheckLogin
 * @package app\admin\middleware
 */
class CheckLogin
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        // 登录校验
        if (!in_array(request()->controller(), ['Login'])) {
            // 获取Token
            $token = request()->header("Authorization");
            if ($token && strpos($token, 'Bearer ') !== false) {
                $token = str_replace("Bearer ", null, $token);
                // JWT解密token
                $jwt = new \Jwt();
                $userId = $jwt->verifyToken($token);
                if (!$userId) {
                    // token解析失败跳转登录页面
                    return json(message("请登录", false, null, 401));
                }
            } else {
                // 跳转至登录页面
                return json(message("请登录", false, null, 401));
            }
        }
        return $next($request);
    }
}

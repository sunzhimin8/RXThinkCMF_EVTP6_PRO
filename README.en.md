# RXThinkCMF_EVTP6_PRO

#### Description
RXThinkCMF_EVTP6_PRO【旗舰版】 基于 ThinkPHP6 + Vue + ElementUI 开发的权限(RBAC)及内容管理框架，框架中集成了权限管理、字典管理、配置管理、城市管理、代码生成器等等常规基础模块，支持多终端适配，完全支持手机端、PAD终端、PC电脑端，可以快速的帮助开发者构建前后端分离框架后台管理系统。专注于为中小企业提供最佳的行业基础后台框架解决方案，执行效率高、扩展性好、稳定性值得信赖，操作体验流畅，欢迎大家来使用！

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
